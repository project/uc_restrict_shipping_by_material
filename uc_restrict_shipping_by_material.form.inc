<?php
// $Id: uc_restrict_shipping_by_material.form.inc,v 1 2011/06/13 11:45:09 craigmc Exp $
/**
 * @file
 * Contains all forms used in the module
 */

/**
 * Menu callback -- ask for confirmation of material restriction deletion
 */
function uc_restrict_shipping_by_material_confirm_remove_restrictions(&$form_state, $material) {
  $form['material'] = array(
    '#type' => 'value',
    '#value' => $material,
  );
	$materials = _uc_restrict_shipping_by_material_get_materials();
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $materials[$material])),
    isset($_GET['destination']) ? $_GET['destination'] : UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute material restriction deletion
 */
function uc_restrict_shipping_by_material_confirm_remove_restrictions_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    _uc_restrict_shipping_by_material_remove_restrictions($form_state['values']['material']);
  }
  $form_state['redirect'] = UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH;
}

/**
 * Menu callback -- ask for confirmation of changing material field
 */
function uc_restrict_shipping_by_material_confirm_change_field(&$form_state, $old_field, $new_field) {
  $form['old_field'] = array(
    '#type' => 'value',
    '#value' => $old_field,
  );
  $form['new_field'] = array(
    '#type' => 'value',
    '#value' => $new_field,
  );

  return confirm_form($form,
    t('Are you sure you want to change from %old_title to %new_title? Doing so will result in deleting all settings based on the old field values.', array('%old_title' => $old_field, '%new_title' => $new_field)),
    isset($_GET['destination']) ? $_GET['destination'] : UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Execute Field change
 */
function uc_restrict_shipping_by_material_confirm_change_field_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
  	_uc_restrict_shipping_by_material_remove_restrictions();

  	// reset cache of materials
  	_uc_restrict_shipping_by_material_get_materials(TRUE);
		variable_set('uc_restrict_shipping_by_material_field_name', $form_state['values']['new_field']);
		drupal_set_message('Cleared settings for <em>'. $form_state['values']['old_field'] .'</em>. Now using: <em>'. $form_state['values']['new_field']  .'</em>. All old settings have been deleted.');
  }
  $form_state['redirect'] = UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH;
}

/**
 * Settings form for specific materials
 * Allows selecting state(s) and entire countrie(s) to disallow shipping
 * 
 * @param Array $form
 * @param String $material
 */
function uc_restrict_shipping_by_material_settings($form, $material) {
	
	$materials = _uc_restrict_shipping_by_material_get_materials();
	
	$form = array();

	drupal_set_title($materials[$material] .': '. drupal_get_title());
	$countries = array();
  $states = array();
	$form['material'] = array(
		'#value' => $materials[$material]
	);  
  
	$countries = _uc_restrict_shipping_by_material_country_list();
	$states = _uc_restrict_shipping_by_material_state_list();

	$restrictions = uc_restrict_shipping_by_material_get_restrictions($material);
	
	$form['material'] = array(
		'#type' => 'value',
		'#value' => $material,
	);
	foreach($countries as $country_id => $country_name) {
		$form[$country_name] = array(
			'#type' => 'fieldset',
			'#collapsible' => TRUE,
			'#collapsed' => FALSE,
			'#title' => $country_name,
		);
		$form[$country_name]['country-'. $country_id] = array(
			'#type' => 'checkbox',
			'#return_value' => $country_id,
			'#title' => $country_name .': All Shipping Restricted for '. $materials[$material],
			'#default_value' => $restrictions[$country_id]['country'] == 'all' ? TRUE : FALSE,
		);
		$form[$country_name]['states-'. $country_id]= array(
			'#type' => 'select',
			'#multiple' => TRUE,
			'#options' => $states[$country_id],
			'#title' => 'States/Provinces',
			'#default_value' => array_keys($restrictions[$country_id]['states']),
			
		);
	}

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save Restrictions'),
	);
	$form['cancel'] = array(
		'#type' => 'submit',
		'#value' => t('Cancel'),
	);
	$form['revert'] = array(
		'#type' => 'submit',
		'#value' => t('Remove All Restrictions'),
	);

	return $form;
}

function uc_restrict_shipping_by_material_settings_submit($form, &$form_state) {
	$values = $form_state['values'];
	switch ($values['op']) {
		case 'Save Restrictions':
	 		// remove all old restrictions-- don't delete material from system settings, just DB values
	 		_uc_restrict_shipping_by_material_remove_restrictions($values['material'], FALSE);

	 		// save restrictions to the DB
	 		$countries = _uc_restrict_shipping_by_material_country_list();
	 		$pk_fields = array('material', 'country_id', 'zone_id');
	 		foreach ($countries as $country_id => $country_name) {
	 			if ($values['country-'. $country_id]) {
		 			$restriction = new stdClass();
		 			$restriction->material = $values['material'];
		 			$restriction->country_id = $country_id;
		 			$restriction->zone_id = 0;
		 			drupal_write_record('uc_restrict_shipping_by_material', $restriction);
	 			}
	 			if (count($values['states-'. $country_id])) {
	 				foreach ($values['states-'. $country_id] as $zone_id) {
	 					$restriction = new stdClass();
		 				$restriction->material = $values['material'];
		 				$restriction->country_id = $country_id;
		 				$restriction->zone_id = $zone_id;
		 				drupal_write_record('uc_restrict_shipping_by_material', $restriction);
	 				}
	 			}
	 		}
	 		// reset cache
	 		uc_restrict_shipping_by_material_get_restrictions($values['material'], TRUE);

		break;
		case 'Cancel':
			$form_state['redirect'] = UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH;
	 	break;
		case 'Remove All Restrictions':
			// redirect to alternate page to handle removing restriction (and confirming action)
			$form_state['redirect'] = UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/remove/'. $values['material'];
		break;
	}
}

/**
 * Form displaying which material(s) have restrictions, and option to add new material(s) to the restrictions list
 */
function uc_restrict_shipping_by_material_admin_settings() {
	$form = array();
  $countries = array();
  $states = array();

  $materials = _uc_restrict_shipping_by_material_get_materials();

  $restricted_materials = variable_get('uc_restrict_shipping_by_material_restricted_materials', array());
  $restricted_items = array();
  $type = content_types('product');
  foreach ($type['fields'] as $key => $field) {
  	$fields[$key] = $key;
  }
  $form['admin_settings'] = array(
		'#type' => 'fieldset',
		'#collapsible' => TRUE,
  	'#collapsed' => TRUE,
		'#title' => 'Admin Settings',
	 );
	 
  if (count($fields) == 0){
  	form_set_error('admin_settings][materials_field', 'You must add a CCK field to the product content type to store materials info. '. l('Manage fields', 'admin/content/node-type/panel/fields'). '.');
		$form['admin_settings']['#collapsed'] = FALSE;
  }

	$form['admin_settings']['materials_field'] = array(
  	'#type' => 'select',
  	'#required' => TRUE,
  	'#title' => 'Materials Field (in Product content type)',
  	'#options' => $fields,
	 	'#default_value' => UC_RESTRICT_SHIPPING_BY_MATERIAL_ALLOWED_FIELD_NAME,
	 	'#description' => 'You must select the CCK field that represents the "material(s)" that you want to restrict shipping on.<br/>Note: Changing this will remove ALL settings from the DB.',
  );
  $form['admin_settings']['save_field'] = array(
  	'#type' => 'submit',
  	'#value' => 'Save Field Settings',
  );
  
	$form['new_restrictions'] = array(
		'#type' => 'fieldset',
		'#collapsible' => FALSE,
		'#title' => 'Restrictions',
	 );
	$form['new_restrictions']['material'] = array(
		'#type' => 'select',
		'#options' => array_diff_key($materials, array_flip($restricted_materials)),
	);
	$form['new_restrictions']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Add Restriction'),
	);
	
	$header = array(
		t('Material'),
		t('Restrictions'),
		'',
		'',
	);
	$rows = array();

	$countries = _uc_restrict_shipping_by_material_country_list();
	$states = _uc_restrict_shipping_by_material_state_list();
	
	foreach($restricted_materials as $material) {
		$info = array();
  	$row = array();
  	$row[] = l($materials[$material], UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/'.  $material);
  	
  	// add listing of current restrictions
  	$restrictions = uc_restrict_shipping_by_material_get_restrictions($material);
		// TODO add JavaScript to easily remove a restriction (click on X to remove)
  	foreach ($restrictions as $country_id => $restriction) {

  		$country_name = $countries[$country_id];
  		if  ($restriction['country'] == 'all') {
  			$info[] = $country_name .': All';
  		}
  		else {
  			$count =  count($restriction['states']);
  			$i = 0;
  			$state_info = '';
  			foreach ($restriction['states'] as $zone_id) {
  				$state_info .= $states[$country_id][$zone_id];
  				if (++$i < $count) {
  					$state_info .= ', ';
  				}
  			}
  			$info[] = $country_name .': '. $state_info;
  		}
  	}
  	$row[] = theme('item_list', $info);
  	$row[] = l('edit', UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/'.  $material);
  	$row[] = l('remove', UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/remove/'. $material);
  	$rows[] = $row;
  }
  $form['restricted_materials'] = array(
		'#type' => 'fieldset',
		'#collapsible' => FALSE,
		'#title' => 'Restricted Materials',
	 );
  $form['restricted_materials'][] = array('#value' => theme('table', $header, $rows, array()));

  return $form;
}

/**
 * Enforce that CCK field exists and is set for materials
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function uc_restrict_shipping_by_material_admin_settings_validate($form, &$form_state)
{
	if (count($form['admin_settings']['materials_field']['#options']) == 0) {
		form_set_error('admin_settings][materials_field', 'You must add a CCK field to the product content type to store materials info. '. l('Manage fields', 'admin/content/node-type/panel/fields'). '.');
	}
	if (!$form_state['values']['materials_field']) {
		form_set_error('admin_settings][materials_field', 'You must select a CCK field to the product content type to store materials info.');
	}
}

/**
 * Submit function for admin settings form.
 * Adds item to system variable and resets menu
 * 
 * @param Array $form
 * @param Array $form_state
 */
function uc_restrict_shipping_by_material_admin_settings_submit($form, &$form_state) {
	switch ($form_state['values']['op']) {
		case 'Add Restriction':
			$restricted_materials = variable_get('uc_restrict_shipping_by_material_restricted_materials', array());
			$restricted_materials[] = $form_state['values']['material'];
			sort($restricted_materials);
			variable_set('uc_restrict_shipping_by_material_restricted_materials', $restricted_materials);
			$form_state['redirect'] = UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/'.  $form_state['values']['material'];
			menu_rebuild();
			break;
		case 'Save Field Settings':
			// Only clear the settings if there 
			if ($form_state['values']['materials_field'] != $form['admin_settings']['materials_field']['#default_value']) {
				// Redirect to confirmation workflow
				$form_state['redirect'] = UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/change_field/'. $form['admin_settings']['materials_field']['#default_value'] .'/'. $form_state['values']['materials_field'];
			}
			break;
	}
}
