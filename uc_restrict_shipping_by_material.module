<?php
// $Id: uc_restrict_shipping_by_material.module,v 1 2011/06/08 11:45:09 craigmc Exp $
/**
 * @file
 * Provides options to restrict shipping to countries, states and/or provinces based on the material(s) contained in the product
 */

define('UC_RESTRICT_SHIPPING_BY_MATERIAL_ALLOWED_FIELD_NAME', variable_get('uc_restrict_shipping_by_material_field_name', 'field_material'));
define('UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH', 'admin/store/settings/restrictshippingmaterial');
/**
 * Check to see if the materials in a given product_nid can be shipped to the requested country_id and zone_id
 * If it is allowed, returns TRUE
 * if not, returns an array of results
 * $result[$material_name] = array(
 *   'country_id' => $country_id,
 *   'zone_id' => $zone_id (or ALL if fails at the country level)
 *   'msg' => descriptive message of the reason for failure
 */
function uc_restrict_shipping_by_material_allowed($product_nid, $country_id, $zone_id) {
  $response = array();
  $countries = _uc_restrict_shipping_by_material_country_list();
  $states = _uc_restrict_shipping_by_material_state_list();

  $materials = _uc_restrict_shipping_by_material_get_product_materials($product_nid);
  $restricted_materials = _uc_restrict_shipping_by_material_get_materials();
  foreach ($materials as $material) {
    $material = _uc_restrict_shipping_by_material_db_key_material($material);
    $restrictions = uc_restrict_shipping_by_material_get_restrictions($material);
    if ($restrictions[$country_id]['country'] == 'all') {
      // restricted at the country level
      // checking at country level
      $response[$material] = array(
        'country_id' => $country_id,
        'zone_id' => 'all',
        'msg' => t('%material is restricted and cannot be shipped to %country', array('%material' => $restricted_materials[$material], '%country' => $countries[$country_id])),
      );
    }
    elseif ($restrictions[$country_id]['states'][$zone_id]) {
      // restricted at the state level
      $response[$material] = array(
        'country_id' => $country_id,
        'zone_id' => $zone_id,
        'msg' => t('%material is restricted and cannot be shipped to %state in %country', array('%material' => $restricted_materials[$material], '%state' => $states[$country_id][$zone_id], '%country' => $countries[$country_id])),
      );
    }
  }// end foreach ($materials as $material)
  return count($response) == 0 ? TRUE : $response;
}

/**
 * Implementation of hook_menu().
 */
function uc_restrict_shipping_by_material_menu() {
  $items = array();
  $items[UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH] = array(
    'title' => 'Restricted Shipping By Material Settings',
    'description' => 'Configure the Restricted Shipping By Material Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_restrict_shipping_by_material_admin_settings'),
    'access arguments' => array('administer store'),
    'file' => 'uc_restrict_shipping_by_material.form.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items[UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/remove'] =  array(
    'title' => 'Restricted Shipping By Material Settings',
    'description' => 'Remove Restrictions for given Material',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_restrict_shipping_by_material_confirm_remove_restrictions', 5),
    'access arguments' => array('administer store'),
    'file' => 'uc_restrict_shipping_by_material.form.inc',
    'type' => MENU_CALLBACK,
  );
  $items[UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/change_field'] =  array(
    'title' => 'Restricted Shipping By Material Settings',
    'description' => 'Change the CCK field used for Materials in product content type',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_restrict_shipping_by_material_confirm_change_field', 5, 6),
    'access arguments' => array('administer store'),
    'file' => 'uc_restrict_shipping_by_material.form.inc',
    'type' => MENU_CALLBACK,
  );
  $materials = variable_get('uc_restrict_shipping_by_material_restricted_materials', array());
  $base_materials = _uc_restrict_shipping_by_material_get_materials();
  foreach ($materials as $material) {
    $items[UC_RESTRICT_SHIPPING_BY_MATERIAL_ADMIN_PATH .'/'. $material] = array(
    'title' => $base_materials[$material],
    'description' => 'Configure the Restricted Shipping By Material Settings for '. $base_materials[$material],
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_restrict_shipping_by_material_settings', $material),
    'access arguments' => array('administer store'),
    'file' => 'uc_restrict_shipping_by_material.form.inc',
    'type' => MENU_LOCAL_TASK,
    );
  }
  return $items;
}

/**
 * Helper function to remove all restrictions for the given material
 * If $material is not specified, clear out all settings
 * @param String $material
 */
function _uc_restrict_shipping_by_material_remove_restrictions($material = '', $remove_from_sysvar = TRUE) {
  $query = 'DELETE FROM {uc_restrict_shipping_by_material}';
  $materials = array();
  $args = array();
  $restrictions = array();
  if ($material) {

    $restrictions = array_flip(variable_get('uc_restrict_shipping_by_material_restricted_materials', array()));
    unset($restrictions[$material]);
    $restrictions = array_flip($restrictions);

    $query .= " WHERE material = '%s'";
    $args[] = $material;
  }
  else {
    // build a list of all affected materials
    $result = db_query('SELECT distinct(material) FROM {uc_restrict_shipping_by_material}');
    while ($row = db_fetch_object($result)){
      $materials[] = $row->material;
    }
  }

  // only remove the system setting if necessary
  if ($remove_from_sysvar) {
    variable_set('uc_restrict_shipping_by_material_restricted_materials', $restrictions);
    menu_rebuild();
  }
  db_query($query, $args);

  // clear the cache for each existing material
  foreach ($materials as $material) {
    // reset cache
    uc_restrict_shipping_by_material_get_restrictions($material, TRUE);
  }
}

/**
 * Helper function to retrieve cached list of countries
 */
function _uc_restrict_shipping_by_material_country_list() {
  $cid = 'uc_restrict_shipping_by_material_country_list';
  $cache = cache_get($cid);
  if ($cache->data) {
    $countries = $cache->data;
  }
  else {
    // Get list of countries
    $result = db_query("SELECT * FROM {uc_countries} WHERE version > 0 ORDER BY country_name ASC");
    while ($country = db_fetch_object($result)) {
      $countries[$country->country_id] = $country->country_name;
    }
    cache_set($cid, $countries, strtotime('+1 week'));
  }
  return $countries;
}

/**
 * Helper function to retrieve cached list of states
 */
function _uc_restrict_shipping_by_material_state_list(){
  $cid = 'uc_restrict_shipping_by_material_state_list';
  $cache = cache_get($cid);
  if ($cache->data) {
    $states = $cache->data;
  }
  else {
    $result = db_query("SELECT * FROM {uc_zones} ORDER BY '%s'", 'zone_name');
    while ($zone = db_fetch_object($result)) {
      $states[$zone->zone_country_id][$zone->zone_id] = $zone->zone_name;
    }
    cache_set($cid, $states, strtotime('+1 week'));
  }
  return $states;
}

/**
 * Function to retrieve the material restrictions for a given material
 * returned as an array:
 * $restrictions[$country_id]['country'] = 'all' -- if blocked at the country level
 * $restrictions[$country_id]['states'][$zone_id] = $zone_id for each state/province level block
 *
 * Uses caching. Pass in $reset = TRUE to reset cache
 *
 * @param String $material
 * @param Boolean $reset
 */
function uc_restrict_shipping_by_material_get_restrictions($material, $reset = FALSE) {
  $cid = _uc_restrict_shipping_by_material_get_restrictions_cid($material);
  $restrictions = array();

  if ($reset) {
    $result = db_query("SELECT * FROM {uc_restrict_shipping_by_material} WHERE material = '%s'", $material);
    while ($row = db_fetch_object($result)) {
      if ($row->zone_id) {
        $restrictions[$row->country_id]['states'][$row->zone_id] = $row->zone_id;
      }
      else {
        $restrictions[$row->country_id]['country'] = 'all';
      }
    }
    cache_set($cid, $restrictions, 'cache', strtotime('+1 week'));
    return $restrictions;
  }

  $cache = cache_get($cid);
  if ($cache->data) {
    return $cache->data;
  }
  else { // Bad cache reset it.
    return uc_restrict_shipping_by_material_get_restrictions($material, TRUE);
  }
}

/**
 * Helper function to generate cid
 *
 * @param String $material
 */
function _uc_restrict_shipping_by_material_get_restrictions_cid($material) {
  return 'uc_restrict_shipping_by_material-restrictions-'. $material;
}

/**
 * Retrieve the material keys/values from the selected CCK field
 * Caches values for 1 week
 *
 * @param Bool $reset
 */
function _uc_restrict_shipping_by_material_get_materials($reset = FALSE) {
  $cid = 'uc_restrict_shipping_by_material_materials';

  if ($reset) {

    $materials = array();
    // Get the list of materials
    module_load_include('inc', 'content', 'includes/content.crud');
    $fields = content_field_instance_read(array('field_name'=>UC_RESTRICT_SHIPPING_BY_MATERIAL_ALLOWED_FIELD_NAME), TRUE);
    foreach ($fields as $field) {
      if ($type_name == 'product') {
        break;
      }
    }
    $values = split("\n", $field['allowed_values']);
    foreach ($values as $value) {
    // Handble both if using named pair or non-keyed material values
      $value = split("\|", $value);
      $key = _uc_restrict_shipping_by_material_db_key_material($value[0]);
      if (count($value) == 1) {
        $materials[$key] = $value[0];
      }
      else {
        $materials[$key] = $value[1];
      }
    }
    cache_set($cid, $materials, 'cache', strtotime('+1 week'));
    return $materials;
  }

  $cache = cache_get($cid);
  if ($cache->data) {
    return $cache->data;
  }
  else { // Bad cache reset it.
    return _uc_restrict_shipping_by_material_get_materials(TRUE);
  }
}

/**
 * Transformation to stored CCK field value to standardize-- in case using a text field value for material
 *
 * @param Mixed $material
 */
function _uc_restrict_shipping_by_material_db_key_material($material) {
  return str_replace(' ', '_', strtolower(trim($material)));
}

/**
 * Helper function to retrieve the material(s) for a given product
 *
 * @param Int $product_nid
 * @param Bool $reset Reset the cache
 */
function _uc_restrict_shipping_by_material_get_product_materials($product_nid, $reset = FALSE) {
  $cid = _uc_restrict_shipping_by_material_get_product_materials_cid($product_nid);

  if ($reset) {

    $materials = array();
    $product = node_load($product_nid);

    $field = UC_RESTRICT_SHIPPING_BY_MATERIAL_ALLOWED_FIELD_NAME;

    foreach ($product->$field as $value) {
      $materials[] = $value['value'];
    }
    if (count($materials) == 0) {
      $materials = FALSE;
    }
    cache_set($cid, $materials, 'cache');
    return $materials;
  }

  $cache = cache_get($cid);
  if ($cache->data) {
    return $cache->data;
  }
  else { // Bad cache reset it.
    return _uc_restrict_shipping_by_material_get_product_materials($product_nid, TRUE);
  }
}

/**
 * Implementation of hook_nodeapi().
 *
 * @param &$node The node the action is being performed on.
 * @param $op What kind of action is being performed. Possible values: alter, delete, delete revision, insert, load, prepare, prepare translation, print, rss item, search result, presave, update, update index, validate, view
 * @param $a3
 * @param $a4
 */
function uc_restrict_shipping_by_material_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if (($node->type == 'product') && ($op == 'update')) {
    // Clear the product materials cache for this nid
    cache_clear_all(_uc_restrict_shipping_by_material_get_product_materials_cid($node->nid));
  }
}

/**
 * Helper function to generate the CID for the product materials cache
 *
 * @param Int $product_nid
 */
function _uc_restrict_shipping_by_material_get_product_materials_cid($product_nid) {
  return 'uc_restrict_shipping_by_material-product-'. $product_nid;
}

/**
 * Implementation of hook_form_alter().
 */
function uc_restrict_shipping_by_material_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_form') {
    // Add additional validation function to checkout process
    array_push($form['#validate'], 'uc_restrict_shipping_by_material_uc_cart_checkout_form_validate');

    // Add additional submit function to redirect person to their cart if there is an issue
    // must happen before other submit functions
    array_unshift($form['#submit'], 'uc_restrict_shipping_by_material_uc_cart_checkout_form_submit');
  }
}

/**
 *
 * Validation function
 * Validates that the product(s) in the cart can all be shipped to the
 * selected delivery address
 *
 * @param $form form to validate
 * @param $form_state A keyed array containing the current state of the form.
 */
function uc_restrict_shipping_by_material_uc_cart_checkout_form_validate($form, &$form_state) {
  $error_count = 0;
  $values = $form_state['values'];
  $cart_contents = unserialize($values['cart_contents']);

  foreach ($cart_contents as $product){
    $response = uc_restrict_shipping_by_material_allowed($product->nid, $values['panes']['delivery']['delivery_country'], $values['panes']['delivery']['delivery_zone']);
    if ($response === TRUE) {
      // do nothing and move on
    }
    else {
      $error_count++;
      if (!isset($_SESSION['uc_restrict_shipping_by_material'])) {
        $_SESSION['uc_restrict_shipping_by_material'] = array();
      }
      $_SESSION['uc_restrict_shipping_by_material']['products'][$product->nid] = $response;
    }
  }
  $form_state['uc_restrict_shipping_by_material']['passed'] = $error_count ? FALSE : TRUE;
}

/**
 * Hijack the cart submission form. If there is an error due to the products not meeting shipping restrictions by material
 * redirect user to the cart page
 *
 * @param $form form to validate
 * @param $form_state A keyed array containing the current state of the form.
 */
function uc_restrict_shipping_by_material_uc_cart_checkout_form_submit($form, &$form_state) {
  if ($_SESSION['checkout_valid'] && !$form_state['uc_restrict_shipping_by_material']['passed']) {
    unset($_SESSION['checkout_valid']);

    foreach ($_SESSION['uc_restrict_shipping_by_material']['products'] as $product_nid => $product) {
      foreach ($product as $material_info) {
        form_set_error('product-'. $product_nid, $material_info['msg']);
      }
    }

    $form_state['redirect'] = 'cart';
    drupal_goto($form_state['redirect']);
  }
}

/**
 * Implementation of hook_tapir_table_alter
 * Ubercart uses tapir_table function to theme panel contents on the
 * Orders pages.
 *
 * add a product-id to the relevant rows
 * if there is an issue with the given product, add an error tag to the row for theming
 *
 * @param Array $table
 * @param String $table_id
 */
function uc_restrict_shipping_by_material_tapir_table_alter(&$table, $table_id) {
  drupal_add_css(drupal_get_path('module', 'uc_restrict_shipping_by_material') .'/css/uc_restrict_shipping_by_material.css');
  // Add product nid class to the /cart products table
  if ($table_id == 'uc_cart_view_table') {
    foreach ($table as $key => $value) {
      if (is_numeric($key)) {
        // ignore the #general attribute items in the array
        if (is_array($value['nid'])) {
          // set an id for the row to help identify via JS, etc.
          $table[$key]['#attributes']['id'] = 'product-'. $value['nid']['#value'];
          if ($_SESSION['uc_restrict_shipping_by_material']['products'][$value['nid']['#value']]) {
            $table[$key]['qty']['#attributes']['class'] .= ' error';
            $table[$key]['remove']['#attributes']['class'] .= ' error';
            $table[$key]['#attributes']['class'] .= ' error';
          }
        }
      }
    }
    unset($_SESSION['uc_restrict_shipping_by_material']);
  }
}